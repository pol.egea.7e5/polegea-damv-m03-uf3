using System;
using System.Collections.Generic;
using System.IO;

namespace GestioDirectoris
{
    /// <summary>
    /// Classe d'entrada i sortida només per als exercicis 1 i 2.
    /// </summary>
    public class InOut
    {
        /// <summary>
        /// Mètode que demana un path per l'usuari.
        /// </summary>
        /// <returns>Retorna el path escrit.</returns>
        public static string Direccio()
        {
            Console.WriteLine("Introdueix el path");
            return @Console.ReadLine();
        }
        /// <summary>
        /// Mètode que retorna un string quan no existeix un path escrit.
        /// </summary>
        public static void NoExisteix()
        {
            Console.WriteLine("No existeix el path");
        }
        /// <summary>
        /// Mètode que retorna adreces absolutes.
        /// </summary>
        /// <param name="num">Aquest paràmetre indica si és un directori o un arxiu.</param>
        /// <param name="path">Aquest paràmetre indica el path d'aquest.</param>
        public static void Absolut(int num,string path)
        {
            switch (num)
            {
                case 1:
                    Console.WriteLine("És un arxiu");
                    Console.WriteLine($"Adreça absoluta: {Path.GetFullPath(path)}");
                    break;
                case 2:
                    Console.WriteLine("És un directori");
                    Console.WriteLine($"Adreça absoluta: {Path.GetFullPath(path)}");
                    break;
            }
        }
        /// <summary>
        /// Mètode que retorna una llista de fitxers.
        /// </summary>
        /// <param name="fitxers">Aquesta llista inclueix tots els fitxers d'un directori</param>
        public static void Fitxers(List<string> fitxers)
        {
            Console.WriteLine("\nFitxers:\n");
            foreach (var fitxer in fitxers)
            {
                Console.WriteLine(fitxer);
            }
        }
        /// <summary>
        /// Mètode que retorna una llista de directoris.
        /// </summary>
        /// <param name="directoris">Aquesta llista inclueix tots els directoris d'un directori</param>
        public static void Directoris(List<string> directoris)
        {
            Console.WriteLine("\nDirectoris:\n");
            foreach (var directori in directoris)
            {
                Console.WriteLine(directori);
            }
        }
    }
}