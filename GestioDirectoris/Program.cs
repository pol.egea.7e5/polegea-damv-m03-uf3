﻿/*
 *  Author: Pol Egea
 *  Date: 11/03/22
 */
using System;

namespace GestioDirectoris
{
    /// <summary>
    /// Classe d'inicialització de programa, amb menús i inicialització de mètodes.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Mètode d'inicialització de programa.
        /// </summary>
        public static void Main()
        {
            do MostraMenu(); while (!TriaMenu());
        }
        /// <summary>
        /// Mètode per a mostrar el menú.
        /// </summary>
        static void MostraMenu()
        {
            Console.WriteLine("1:Exercici1");
            Console.WriteLine("2:Exercici2");
            Console.WriteLine("3:Exercici3");
            Console.WriteLine("4:Exercici4");
            Console.WriteLine("FI:Sortir del programa");
        }
        /// <summary>
        /// Mètode de tria de mètode.
        /// </summary>
        /// <returns>Retorna true en cas que es vulgui sortir del programa.</returns>
        static bool TriaMenu()
        {
            bool fi = false;
            string exercici = Console.ReadLine();
            switch (exercici)
            {
                case "1":
                    Exercici1();
                    break;
                case "2":
                    Exercici2();
                    break;
                case "3":
                    Exercici3();
                    break;
                case "4":
                    Exercici4();
                    break;
                case "FI":
                    fi=true;
                    break;

            }
            return fi;
        }
        /// <summary>
        /// Mètode d'inicialització del exercici1.
        /// </summary>
        public static void Exercici1()
        {
            string path = InOut.Direccio();
            if(!Exercicis.Metode1(path))InOut.NoExisteix();
        }
        /// <summary>
        /// Mètode d'inicialització del exercici2.
        /// </summary>
        public static void Exercici2()
        {
            string path = InOut.Direccio();
            Exercicis.Metode2(path);
        }
        /// <summary>
        /// Mètode d'inicialització del exercici3.
        /// </summary>
        public static void Exercici3()
        {
            string path = InOut.Direccio();
            Exercicis.Metode3(path);

        }
        /// <summary>
        /// Mètode d'inicialització del exercici4.
        /// </summary>
        public static void Exercici4()
        {
            string path = InOut.Direccio();
            Exercicis.Metode4(path);
        }
    }
}