using System;
using System.Collections.Generic;
using System.IO;

namespace GestioDirectoris
{
    /// <summary>
    /// Classe on es troben tots els mètodes dels exercicis.
    /// </summary>
    public class Exercicis
    {
        /// <summary>
        /// Mètode que busca si existeix un path i si es fitxer o directori.
        /// </summary>
        /// <param name="path">String de l'adreça.</param>
        /// <returns>Retorna true si existeix, fals si no existeix.</returns>
        public static bool Metode1(string path)
        {
            if (File.Exists(path))
            {
                InOut.Absolut(1,path);
                return true;
            } 
            if (Directory.Exists(path))
            {
                InOut.Absolut(2,path);
                return true;
            }

            return false;

        }
        /// <summary>
        /// Mètode que busca tots els fitxers i directoris d'una carpeta.
        /// </summary>
        /// <param name="path">Adreça a buscar.</param>
        public static void Metode2(string path)
        {
            if (Directory.Exists(path))
            {
                List<string> directoris = new List<string>(Directory.EnumerateDirectories(path));
                List<string> fitxers = new List<string>(Directory.EnumerateFiles(path));
                InOut.Directoris(directoris);
                InOut.Fitxers(fitxers);
            }
            else InOut.NoExisteix();
        }
        /// <summary>
        /// Mètode que ens permet crear,canviar el nom i eliminar carpetes d'un directori.
        /// </summary>
        /// <param name="path">Adreça on modificar les carpetes.</param>
        public static void Metode3(string path)
        {
            if (Directory.Exists(path))
            {
                string ordre,carpeta,carpeta2;
                do
                {
                    Directory.SetCurrentDirectory(Path.GetFullPath(path));
                    Console.WriteLine("\nTria funció: CREATE, RENAME, DELETE o FI\n");
                    ordre = Console.ReadLine();
                    switch (ordre)
                    {
                        case "CREATE":
                            Console.WriteLine("Introdueix nom");
                            carpeta = Console.ReadLine();
                            if (carpeta != null) Directory.CreateDirectory(carpeta);
                            break;
                        case "RENAME":
                            Console.WriteLine("Introdueix nom carpeta a fer el rename");
                            carpeta = Console.ReadLine();
                            Console.WriteLine("Introdueix nom carpeta nou");
                            carpeta2 = Console.ReadLine();
                            if (carpeta != null&&carpeta2!=null) Directory.Move(carpeta,carpeta2);
                            break;
                        case "DELETE":
                            Console.WriteLine("Introdueix nom de la carpeta a borrar");
                            carpeta = Console.ReadLine();
                            if (carpeta != null) Directory.Delete(carpeta);
                            break;
                    }

                } while (ordre != "FI");
            }
            else InOut.NoExisteix();
        }
        /// <summary>
        /// Mètoda que busca els arxius i carpetes, i mostra els fitxers i directoris recursivament dels directoris actuals, el resultat mostra dades importants dels fitxers i carpetes.
        /// </summary>
        /// <param name="path">Carpeta on es busquen els arxius i els directoris.</param>
        public static void Metode4(string path)
        {
            if (Directory.Exists(path))
            {
                var infodir = new DirectoryInfo(path);
                FileInfo[] fitxers = infodir.GetFiles();
                DirectoryInfo[] directoris = infodir.GetDirectories();
                Console.WriteLine("\nFitxers de {0}\n",path);
                foreach (var fitxer in fitxers)
                {
                    Console.WriteLine($"Nom: {fitxer.Name}, Tamany: {fitxer.Length}, Data modificació: {fitxer.LastWriteTime}");
                }
                Console.WriteLine("\nDirectoris de {0}\n",path);
                foreach (var directori in directoris)
                {
                    Console.WriteLine($"Nom: {directori.Name}, Directori: Si, Data modificació: {directori.LastWriteTime}");
                    Metode4(Path.Combine(path,directori.Name));
                }
            }
            else InOut.NoExisteix();
        }
    }
}